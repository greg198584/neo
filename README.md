# Neo

This project aims to create a personal assistant that can perform a wide range of tasks related to computer usage and software development. The assistant will use natural language processing and machine learning to understand voice commands and carry out actions such as searching the web, creating reminders, sending emails, and performing software development tasks. The goal is to create a flexible and customizable assistant that can streamline workflows and improve productivity for users.

The project consists of three main components: voice recognition, natural language processing, and text-to-speech synthesis. The voice recognition component uses the SpeechRecognition library to capture and transcribe the user's spoken commands. The natural language processing component utilizes the OpenAI API to understand the user's intent and generate a response. Finally, the text-to-speech synthesis component uses the gTTS library and pygame mixer to generate and play back an audio response.

## Installation

Describe how to install and run your project.

```bash
$ git clone https://gitlab.com/greg198584/neo.git
$ cd <your-repository>
$ pip install -r requirements.txt
```

## Usage

```bash
$ python3 main.py
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Authors

* [Greg Lafitte](https://gitlab.com/greg198584) - Initial work

