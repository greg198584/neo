# chatgpt.py

import openai
# from init import openai
from config import OPENAI_API_KEY
openai.api_key = OPENAI_API_KEY


def send_request_to_gpt(prompt, context=None):
    if context:
        prompt = context + prompt

    try:
        response = openai.Completion.create(
            engine="text-davinci-003",
            prompt=prompt,
            max_tokens=2048,
            n=1,
            stop=None,
            temperature=0.5,
        )
        return response["choices"][0]["text"]
    except openai.error.RateLimitError as e:
        print("Error: Rate limit exceeded. Please check your plan and billing details.")
        return "Je suis désolé, je ne peux pas répondre pour le moment. Veuillez réessayer plus tard."
    except Exception as e:
        print("Error: ", e)
        return "Je suis désolé, une erreur est survenue. Veuillez réessayer plus tard."
